<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Proposal;
use App\Image;
use App\Page;
use App\Module;
use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProposalController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @param Project $project
     * @param Module $module
     * @param Page $page
     * @param Image $image
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request, Project $project, Module $module, Page $page, Image $image)
    {
        $proposal = new Proposal();
        $image->proposals()->save($proposal);
        return redirect()->route('project.module.page.show', [$project, $module, $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ImageRequest $request
     * @param Project $project
     * @param Module $module
     * @param Page $page
     * @param Image $image
     * @param Proposal $proposal
     * @return \Illuminate\Http\Response
     */
    public function update(ImageRequest $request, Project $project, Module $module, Page $page, Image $image, Proposal $proposal)
    {
        $proposal->update( $request->all() );
        $proposal->save();
        return redirect()->route('project.module.page.show', [$project, $module, $page]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @param Module $module
     * @param Page $page
     * @param Image $image
     * @param Proposal $proposal
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Project $project, Module $module, Page $page, Image $image, Proposal $proposal)
    {
        $proposal->delete();
        return redirect()->route('project.module.page.image.show', [$project, $module, $page, $image]);
    }
}

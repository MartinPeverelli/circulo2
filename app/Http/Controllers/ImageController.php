<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Image;
use App\Page;
use App\Module;
use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @param Project $project
     * @param Module $module
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request, Project $project, Module $module, Page $page)
    {
        $image = new Image();
        $page->images()->save($image);
        return redirect()->route('project.module.page.show', [$project, $module, $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ImageRequest $request
     * @param Project $project
     * @param Module $module
     * @param Page $page
     * @param Image $image
     * @return \Illuminate\Http\Response
     */
    public function update(ImageRequest $request, Project $project, Module $module, Page $page, Image $image)
    {
        $image->update( $request->all() );
        $image->save();
        return redirect()->route('project.module.page.show', [$project, $module, $page]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @param Module $module
     * @param Page $page
     * @param Image $image
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Project $project, Module $module, Page $page, Image $image)
    {
        $image->delete();
        return redirect()->route('project.module.page.show', [$project, $module, $page]);
    }
}

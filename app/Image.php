<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'type', 'status', 'request'];

    /**
     * Get the page that owns the image.
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    /**
     * Get all of the images for the page.
     */
    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    /**
     * Get the page that owns the image.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

@extends('app')

@section('content')

    @include('partials.breadcrumbs')

    <div class="row">
        <div class="col-md-12">
            <h1>{{ $project->name }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <p><strong>ISBN:</strong> {{ $project->isbn }}</p>
        </div>
        <div class="col-md-4">
            <p><strong>Planned modules:</strong> {{ $project->planned_modules }}</p>
        </div>
        <div class="col-md-4">
            <p><strong>Planned pages:</strong> {{ $project->planned_pages }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p><strong>Authors:</strong> {{ $project->authors }}</p>
        </div>
        <div class="col-md-6">
            <p><strong>Format:</strong> {{ $project->format }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p><strong>Alternative format:</strong> {{ $project->alternative_format }}</p>
        </div>
        <div class="col-md-6">
            <p><strong>Complementary materials:</strong> {{ $project->complementary_materials }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p><strong>Notes:</strong> {{ $project->notes }}</p>
        </div>
    </div>

    <hr/>

    <div class="row">
        <div class="col-md-12">

            <h2>
                Modules
                <small class="pull-right">
                    {!! link_to_route('project.module.create', 'New Module', [$project], ['class' => 'btn btn-success']) !!}
                </small>
            </h2>

            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Pages</th>
                    <th>Planned pages</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($project->rootModules as $module)
                    <tr>
                        <td>{!! link_to_route('project.module.show', $module->name, [$project, $module], ['class' => '']) !!}</td>
                        <td>{{ $module->pages()->count() }}</td>
                        <td>{{ $module->planned_pages }}</td>
                        <td class="shrink">{!! link_to_route('project.module.edit', 'Edit', [$project, $module], ['class' => 'btn btn-xs btn-primary']) !!}</td>
                        <td class="shrink">{!! link_to_route('project.module.submodule.create', 'New Submodule', [$project, $module], ['class' => 'btn btn-xs btn-success']) !!}</td>
                        <td class="shrink">
                            {!! Form::open(['route' => ['project.module.destroy', $project, $module], 'method' => 'delete']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @foreach($module->submodules as $submodule)
                        @include('project._submodules', ['module' => $submodule, 'count' => 1])
                    @endforeach
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection
@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">
            <h1>
                Projects
                <small class="pull-right">
                    {!! link_to_route('project.create', 'New', [], ['class' => 'btn btn-success']) !!}
                </small>
            </h1>
        </div>
        <div class="col-md-12">

            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>ISBN</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                <tr>
                    <td>{!! link_to_route('project.show', $project->name, [$project], ['class' => '']) !!}</td>
                    <td>{{ $project->isbn }}</td>
                    <td class="shrink">{!! link_to_route('project.edit', 'Edit', [$project], ['class' => 'btn btn-xs btn-primary']) !!}</td>
                    <td class="shrink">
                        {!! Form::open(['route' => ['project.destroy', $project->id], 'method' => 'delete']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </div>

@endsection
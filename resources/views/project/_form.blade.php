<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', $project->name, ['class' => 'form-control', 'placeholder' => 'Project Name']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('isbn') ? 'has-error' : '' }}">
    {!! Form::label('isbn', 'ISBN', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('isbn', $project->isbn, ['class' => 'form-control', 'placeholder' => 'ISBN']) !!}
        {!! $errors->first('isbn', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('planned_modules') ? 'has-error' : '' }}">
    {!! Form::label('planned_modules', 'Planned modules', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('planned_modules', $project->planned_modules, ['class' => 'form-control', 'placeholder' => 'Planned modules']) !!}
        {!! $errors->first('planned_modules', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('planned_pages') ? 'has-error' : '' }}">
    {!! Form::label('planned_pages', 'Planned pages', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('planned_pages', $project->planned_pages, ['class' => 'form-control', 'placeholder' => 'Planned pages']) !!}
        {!! $errors->first('planned_pages', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('authors') ? 'has-error' : '' }}">
    {!! Form::label('authors', 'Authors', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('authors', $project->authors, ['class' => 'form-control', 'placeholder' => 'Authors']) !!}
        {!! $errors->first('authors', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('format') ? 'has-error' : '' }}">
    {!! Form::label('format', 'Format', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('format', $project->format, ['class' => 'form-control', 'placeholder' => 'Format']) !!}
        {!! $errors->first('format', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('alternative_format') ? 'has-error' : '' }}">
    {!! Form::label('alternative_format', 'Alternative format', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('alternative_format', $project->alternative_format, ['class' => 'form-control', 'placeholder' => 'Alternative format']) !!}
        {!! $errors->first('alternative_format', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('complementary_materials') ? 'has-error' : '' }}">
    {!! Form::label('complementary_materials', 'Complementary materials', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('complementary_materials', $project->complementary_materials, ['class' => 'form-control', 'placeholder' => 'Complementary materials']) !!}
        {!! $errors->first('complementary_materials', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
    {!! Form::label('notes', 'Notes', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('notes', $project->notes, ['class' => 'form-control', 'placeholder' => 'Notes']) !!}
        {!! $errors->first('notes', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit('Save', ['class' => 'btn btn-success pull-right']) !!}
    </div>
</div>
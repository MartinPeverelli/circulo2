<div class="form-group">
    {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('status', ['author' => 'Author', 'draft' => 'Draft', 'layout' => 'Layout', 'prepress' => 'Prepress', 'close' => 'Close'], $page->status, ['class' => 'form-control']) !!}
    </div>
    {!! Form::label('layout_status', 'Layout Status', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('layout_status', ['author' => 'Author', 'draft' => 'Draft', 'layout' => 'Layout', 'prepress' => 'Prepress', 'close' => 'Close'], $page->layout_status, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-2">
        {!! Form::submit('Save', ['class' => 'btn btn-success pull-right']) !!}
    </div>
</div>
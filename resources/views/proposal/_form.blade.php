<tr>
    <td>{!! Form::text('name', $proposal->name, ['class' => 'form-control input-sm', 'placeholder' => 'Proposal Name']) !!}</td>
    <td>{!! Form::select('selected', [0 => 'No', 1 => 'Yes'], $proposal->selected, ['class' => 'form-control input-sm']) !!}</td>
    <td>{!! Form::select('downloaded', [0 => 'No', 1 => 'Yes'], $proposal->downloaded, ['class' => 'form-control input-sm']) !!}</td>
    <td>{!! Form::select('rejected', [0 => 'No', 1 => 'Yes'], $proposal->rejected, ['class' => 'form-control input-sm']) !!}</td>
    <td>{!! Form::text('url', $proposal->url, ['class' => 'form-control input-sm', 'placeholder' => 'URL']) !!}</td>
    <td class="shrink">{!! Form::submit('Save', ['class' => 'btn btn-sm btn-success']) !!}</td>
</tr>
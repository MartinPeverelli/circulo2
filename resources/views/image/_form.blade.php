<div class="form-group">
    <div class="col-sm-4">
        {!! Form::text('user_id', $image->user_id, ['class' => 'form-control', 'placeholder' => 'User']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::select('type', ['photo' => 'Photo', 'illustration' => 'Illustration', 'montage' => 'Montage', '3d' => '3D', 'vector' => 'Vector'], $image->type, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::select('status', ['search' => 'Search', 'selection' => 'Selection', 'draft' => 'Draft', 'color' => 'Color', 'download' => 'Download', 'finishing' => 'Finishing', 'retouch' => 'Retouch', 'approval' => 'Approval'],  $image->status, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10">
        {!! Form::textarea('request', $page->request, ['class' => 'form-control', 'placeholder' => 'Request', 'rows' => 3]) !!}
    </div>
    <div class="col-sm-2">
        {!! Form::submit('Save', ['class' => 'btn btn-success pull-right']) !!}
    </div>
</div>